#!/usr/bin/env python

import tweepy
import json
import jsonpickle
import os
import pandas as pd
import numpy as np
import io
import re
from optparse import OptionParser
from textblob.sentiments import NaiveBayesAnalyzer
from tweepy import OAuthHandler
from geopy.geocoders import Nominatim

company = ""
twitter_handle = ""
parser = OptionParser()
parser.add_option("-c", "--company", dest="company",
                  help="company")
parser.add_option("-t", "--twitter_handle", dest="twitter_handle",
                  help="twitter handle, does not need the @ sign")
parser.add_option("-n", "--num_of_tweets", dest="num_of_tweets", default=100,
                  help="number of tweets to return per company")

(options, args) = parser.parse_args()

print("company = {0}".format(options.company))
print("twitter_handle = {0}".format(options.twitter_handle))
print("num_of_tweets = {0}".format(options.num_of_tweets))

usernames = []
if options.company:
    if twitter_handle:
        handle = '@' + options.twitter_handle
        usernames = [handle]
    else:
        handle = '@' + options.company
        usernames = [handle]
else:
    companies = pd.read_csv('airlinelist.csv')
    usernames = companies['Twitter'].tolist()

print(usernames)

consumer_key = os.environ['consumer_key']
consumer_secret = os.environ['consumer_secret']
access_token = os.environ['access_token']
access_secret = os.environ['access_secret']
auth = OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_secret)
 
api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)
retweet_filter='-filter:retweets'
my_list_of_dicts = []

def get_tweets(searchQuery, fName) :
    tweetsPerQry = int(options.num_of_tweets)
    sinceId = None
    max_id = -1
    maxTweets = int(options.num_of_tweets)

    tweetCount = 0
    print("Downloading max {0} tweets".format(maxTweets))
    with open(fName, 'w') as f:
        while tweetCount < maxTweets:
            try:
                if (max_id <= 0):
                    if (not sinceId):
                        new_tweets = api.search(q=searchQuery+retweet_filter, count=tweetsPerQry,
                                                tweet_mode="extended")
                    else:
                        new_tweets = api.search(q=searchQuery+retweet_filter, count=tweetsPerQry,
                                                since_id=sinceId, tweet_mode="extended")
                else:
                    if (not sinceId):
                        new_tweets = api.search(q=searchQuery+retweet_filter, count=tweetsPerQry,
                                                max_id=str(max_id - 1),
                                                tweet_mode="extended")
                    else:
                        new_tweets = api.search(q=searchQuery+retweet_filter, count=tweetsPerQry,
                                                max_id=str(max_id - 1),
                                                since_id=sinceId,
                                                tweet_mode="extended")
                if not new_tweets:
                    print("No more tweets found")
                    break
                for tweet in new_tweets:
                    my_list_of_dicts.append(tweet._json)

                f.write(json.dumps(my_list_of_dicts, indent=4))
                tweetCount += len(new_tweets)
                print("Downloaded {0} tweets".format(tweetCount))
                max_id = new_tweets[-1].id
            except tweepy.TweepError as e:
                # Just exit if any error
                print("some error : " + str(e))
                break
    return tweetCount

fName = "data/tweets.json"
if options.company:
    fName = "data/tweets_" + options.company + ".json"

totalTweets = 0

for username in usernames:
    tweets = get_tweets(username, fName)
    totalTweets += tweets

print ("Downloaded {0} tweets, Saved to {1}".format(totalTweets, fName))
